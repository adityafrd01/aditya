<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta http-equiv="X-UA-Compatible" content="ie=edge">
 <title>Array</title>
</head>
<body>
 <h1>Berlatih Array</h1>
 <?php
 echo "<h3> Soal 1 </h3>";
 /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
 $kids = array("mike", "Dustin", "will", "lucas", "max", "eleven");
 $adults= array ("hopper", "nancy", "joyce", "jonathan", "murray");
echo "kids : ".$kids[0].", " .$kids[1]. ", ".$kids[2].", ".$kids[3].", ".$kids[4].", ".$kids[5];
echo "</br>";
 echo "adults : ".$adults[0].", " .$adults[1]. ", ".$adults[2].", ".$adults[3].", ".$adults[4];
 echo "<h3> Soal 2</h3>";
 /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
echo "Cast Stranger Things: ";
echo count($kids);
echo "<br>";
echo "Total Kids: "; // Berapa panjang array kids
echo "<br>";
echo "<ol>";
echo "<li> $kids[0] </li>";
echo "<li> $kids[1] </li>";
echo "<li> $kids[2] </li>";
echo "<li> $kids[3] </li>";
echo "<li> $kids[4] </li>";
echo "<li> $kids[5] </li>";
// Lanjutkan
 echo "</ol>";
 echo "Total Adults: "; // Berapa panjang array adults
echo count($adults);
 echo "<br>";
 echo "<ol>";
echo "<li> $adults[0] </li>";
echo "<li> $adults[1] </li>";
echo "<li> $adults[2] </li>";
echo "<li> $adults[3] </li>";
echo "<li> $adults[4] </li>";
// Lanjutkan
 echo "</ol>";
/*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"
            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"
            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"
            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"

            Output:
            Array
                (
                    [0] => Array
                        (
                            [Name] => Will Byers
                            [Age] => 12
                            [Aliases] => Will the Wise
                            [Status] => Alive
                        )
                    [1] => Array
                        (
                            [Name] => Mike Wheeler
                            [Age] => 12
                            [Aliases] => Dugeon Master
                            [Status] => Alive
                        )
                    [2] => Array
                        (
                            [Name] => Jim Hooper
                            [Age] => 43
                            [Aliases] => Chief Hopper
                            [Status] => Deceased
                        )
                    [3] => Array
                        (
                            [Name] => Eleven
                            [Age] => 12
                            [Aliases] => El
                            [Status] => Alive
                        )
                )
            
        */
echo "<h3> soal 3</h3>";
$kids1 = array (
    array("will byers",12,"will the wise","alive"),
    array("mike wheeler",12,"dungeon master","alive"),
    array("jim hopper",43,"chief hopper","deceased"),
    array("eleven",12,"el","alive")
);
echo "Name : ".$kids1[0][0]."</br>Age : ".$kids1[0][1]."</br>Aliases: ".$kids1[0][2]."</br>status : ".$kids1[0][3]."</br>";
echo "Name : ".$kids1[1][0]."</br>Age : ".$kids1[1][1]."</br>Aliases: ".$kids1[1][2]."</br>status : ".$kids1[1][3]."</br>";
echo "Name : ".$kids1[2][0]."</br>Age : ".$kids1[2][1]."</br>Aliases: ".$kids1[2][2]."</br>status : ".$kids1[2][3]."</br>";
echo "Name : ".$kids1[3][0]."</br>Age : ".$kids1[3][1]."</br>Aliases: ".$kids1[3][2]."</br>status : ".$kids1[3][3]."</br>";

    ?>
</body>
</html>